def tau(n):
    t=2
    p=1
    c=1
    if n==1:
        return 1
    while n!=1:
        if n%t==0:
            n=n/t
            c+=1
        else:
            t+=1
            p*=c
            c=1
        if n==1:
            p*=c
    return p

def solution(x):
    s=1
    n=1
    while tau(s)<=x:
        n+=1
        s+=n
    return s
print(solution(500))
