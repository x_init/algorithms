fi = open('input.txt', 'r')
inp = fi.read()
fi.close()

summ = 0
for i in range(len(inp) - 4):
	st = inp[i] + inp[i + 1] + inp[i + 2] + inp[i + 3] + inp[i + 4] 
	if st == '>>-->' or st == '<--<<':
		summ += 1

fo = open('output.txt', 'w')
fo.write(str(summ))
fo.close()
