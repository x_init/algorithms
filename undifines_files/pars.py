#import asyncio
import requests
from bs4 import BeautifulSoup as BS
#from fake_useragent import UserAgent
import lxml

headers = {
    'Host': 'ru.wikipedia.org',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0',
    'Accept': 'application/json; charset=utf-8; profile="https://www.mediawiki.org/wiki/Specs/Summary/1.2.0"',
    'Accept-Language': 'ru',
    'Accept-Encoding': 'gzip, deflate, br',
    'X-Requested-With': 'XMLHttpRequest',
    'Connection': 'keep-alive',
    'DNT': '1',
    'Referer': 'https://www.google.com/',
    'Sec-GPC': '1'
}

url = 'https://ru.wikipedia.org/w/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%96%D0%B8%D0%B2%D0%BE%D1%82%D0%BD%D1%8B%D0%B5_%D0%BF%D0%BE_%D0%B0%D0%BB%D1%84%D0%B0%D0%B2%D0%B8%D1%82%D1%83&from=%D0%90' 

# нет буквы 'Ё'
rus = 'АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЭЮЯ'
end =  'https://ru.wikipedia.org/w/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%96%D0%B8%D0%B2%D0%BE%D1%82%D0%BD%D1%8B%D0%B5_%D0%BF%D0%BE_%D0%B0%D0%BB%D1%84%D0%B0%D0%B2%D0%B8%D1%82%D1%83&pagefrom=Abrocomophaga+emmons&subcatfrom=%D0%90&filefrom=%D0%90#mw-pages'




def getting_reference_and_array(url):
    r = requests.get(url, headers=headers)
    r.encoding = 'utf-8'
    html = BS(r.text, 'lxml')


    lst_animals = [[v.get_text() for v in el.find_all('li')] for el in html.find_all('div', class_='mw-category-group')]

    url = html.find('div', id='mw-pages').find_all('a')
    url = url[len(url) - 1].get('href')

    return  ( url, lst_animals )

 

sp = [] # хранит колличество видов на каждую букву алфавита

acc = 0 # счётчик в бесконечном цикле
s = 0   # хранит длину одного массива спаршенного с одной или боллее страниц


print('App start')

while True:
    data = getting_reference_and_array(url)
    url = 'https://ru.wikipedia.org' + data[0]
    data = data[1]
    
    # на одной странице могут быть несколько разлчиных массивов
    for element in data:
        if rus[acc] == element[0][0]:
            s += len(element)

        else:
            sp.append(s)
            s = 0
            s += len(element)
            print(rus[acc], sp[acc])
            acc += 1
    
    #print()
    if url == end:
        break


print(sp)

