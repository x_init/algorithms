#дружественные числа
from math import sqrt

def d(n):
    s=0
    acc=2
    while acc*acc<n:
        if n%acc==0:
            s+=(acc+n//acc)
        acc+=1
    return s+1


s=0
for i in range(10**4):
    if i==d(d(i)) and i!=d(i):
        s+=i
        print(i, d(i), d(d(i)))
print(s)




