fi = open('input.txt', 'r')
string = fi.read()
fi.close()
string = string.split(' ')
if int(string[0]) <= 10**2 and int(string[1]) <= 10**2 and int(string[2]) <= 10**6:
	a = int(string[0])
	b = int(string[1])
	c = int(string[2])
	if a*b==c:
		ss = 'YES'
	else:
		ss = 'NO'

fo = open('output.txt', 'w')
fo.write(str(ss) + '\n')
fo.close()
