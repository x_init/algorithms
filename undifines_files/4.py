
#string = input('type string: ')
fi = open('INPUT.txt', 'r')
string = fi.read()
fi.close()


if string[-1] == '\n':
    string = string[:-1:]

#string = string.upper()
#print(string)
#print(ord('a'))
#print(ord('z'))
#print(len(string))
#print(len(string) != 5)
#print(string[2] != '-')
#print(int(string[1]) > 8)
#print(int(string[4]) > 8)
#print(not (ord(string[0]) >= 65 and ord(string[0]) <= 72)) 
#print(not (ord(string[3]) >= 65 and ord(string[3]) <= 72))
#print(int(string[1]) == 0)
#print(int(string[4]) == 0)

if len(string) != 5 or string[2] != '-' or\
        int(string[1]) > 8 or int(string[4]) > 8 or\
        (not (ord(string[0]) >= 65 and ord(string[0]) <= 72)) or (not (ord(string[3]) >= 65 and ord(string[3]) <= 72)) or\
        int(string[1]) == 0 or int(string[4]) == 0:

    print('ERROR')
    answer = 'ERROR'
elif (abs(int(string[1]) - int(string[4])) == 2 and abs(ord(string[0]) - ord(string[3])) == 1) ^\
        (abs(int(string[1]) - int(string[4])) == 1 and abs(ord(string[0]) - ord(string[3])) == 2): 
    print('YES')
    answer = 'YES'
else:
    print('NO')
    answer = 'NO'

fo = open('OUTPUT.txt', 'w')
fo.write(answer)
fo.close()




