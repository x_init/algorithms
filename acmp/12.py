with open('input.txt', 'r') as fi:
    data = fi.read()
 
data = [[int(e1) for e1 in e.split(' ') if len(e1) > 0] for e in data.split('\n') if len(e) > 0]
n = data[0][0]
lots_of = set()

count = 0
def check(data):
    global count
    for el in data:
        for e in el:
            if abs(e) > 5**4:
                return 1
    return 0
            
acc = 0
if (n >= 1 and n <= 1000) and (check(data) == 0):
    data = data[1:]
    user_data = [user_data[:2:] for user_data in data]
    home_data = [home_data[2::] for home_data in data]
 
    for sub_user_data, sub_home_data in zip(user_data, home_data):
        #print('цикл: {}'.format(acc))
        x1, y1, x2, y2, x3, y3, x4, y4 = sub_home_data
        
        x, y = sub_user_data
        acc += 1
        if ((x1, y1) == (x2, y2) == (x3, y3) == (x4, y4)):
            if (x1, y1) == (x, y):
                count += 1
                #print('точка на точке')
            else:
                #print('точка не на точке')
                pass

            #print('точка')
            continue
        elif ((x1, y1) == (x2, y2) and (x3, y3) == (x4, y4)):
            if (x - x1) * (y3 - y1) == (x3 - x1) * (y - y1):
                #print('точка на прямой 1')
                if x >= min(x1, x3) and x <= max(x1, x3) and y >= min(y1, y3) and  y <= max(y1, y3):
                    #print('точка на отрезке 1')
                    count += 1
            else:
                #print('точка не на прямой 1')
                pass
            continue

        elif ((x1, y1) == (x4, y4) and (x2, y2) == (x3, y3)):
            if (x - x1) * (y2 - y1) == (x2 - x1) * (y - y1):
                #print('точка на прямой 2')
                if x >= min(x1, x2) and x <= max(x1, x2) and y >= min(y1, y2) and  y <= max(y1, y2):
                    #print('точка на отрезке 1')
                    count += 1
            else:
                pass
                #print('точка не на прямой 2')
            continue

        else:
            #print('прямоугольник')
            pass

        a1 = (x1 - x2)
        b1 = (y1 - y2)
        c1 = (a1**2 + b1**2)**0.5
 
        a2 = (x2 - x3)
        b2 = (y2 - y3)
        c2 = (a2**2 + b2**2)**0.5
 
        #print('{0} - a\n{1} - b\n'.format(c1, c2))
        S = c1 * c2
        x, y = sub_user_data
 
        s1 = 0.5 * abs( ((x - x2) * (y - y3)) - ((x - x3) * (y - y2)) )
        s2 = 0.5 * abs( ((x - x3) * (y - y4)) - ((x - x4) * (y - y3)) )
        s3 = 0.5 * abs( ((x - x4) * (y - y1)) - ((x - x1) * (y - y4)) )
        s4 = 0.5 * abs( ((x - x1) * (y - y2)) - ((x - x2) * (y - y1)) )
        
        #print('S = {}'.format(S))
        #print('S(summ) = {}'.format(s1 + s2 + s3 + s4))
        S_res = s1 + s2 + s3 + s4
        if S_res == S:
            count += 1


        #print('result: ', count, '----------------------------')
    with open('output.txt', 'w') as fo:
        fo.write(str(count)) 
